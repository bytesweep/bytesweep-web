#!/usr/bin/env python
import psycopg2
import json
import bcrypt
from ruamel.yaml import YAML

def dbconnect():
	config_file = open('/etc/bytesweep/config.yaml','r')
	yaml=YAML()
	config = yaml.load(config_file.read())
	config_file.close()
	try:
		conn = psycopg2.connect(host=config['dbhost'],dbname=config['dbname'], user=config['dbuser'], password=config['dbpass'])
		return conn
	except:
		print('ERROR: unable to connect to the database')
		return False

def dbclose(conn):
	conn.close()


def init_tables():
	js = init_job_status_type()
	j = init_jobs_table()
	d = init_data_table()
	u = init_users_table()
	g = init_groups_table()
	ug = init_user_group_table()
	cve = init_cve_table()
	cvem = init_cve_metadata_table()
	watch = init_watchdog_3p_cve_table()
	watchm = init_watchdog_metadata_table()
	return js and j and d and u and g and ug and cve and cvem and watch and watchm

def init_job_status_type():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	# TODO check for existance of enum
	cur.execute("CREATE TYPE job_status AS ENUM ('queued', 'processing', 'done')")
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def db_is_setup():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='jobs')")
	if cur.fetchone()[0]:
		return True
	else:
		return False

def init_watchdog_3p_cve_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='watchdog_3p_cve')")
	if not cur.fetchone()[0]:
		cur.execute("create table watchdog_3p_cve (id serial NOT NULL primary key,data_id serial references data(data_id),cve_id serial references cve(cve_id),discover_date timestamp,last_modified_date_text text,new boolean NOT NULL DEFAULT FALSE, rule_matches jsonb,match_id int)")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def init_watchdog_metadata_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='watchdog_metadata')")
	if not cur.fetchone()[0]:
		cur.execute("create table watchdog_metadata (id serial NOT NULL primary key,last_modified_date timestamp, last_modified_date_text text)")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def init_cve_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='cve')")
	if not cur.fetchone()[0]:
		cur.execute("create table cve (cve_id serial NOT NULL primary key,data jsonb)")
		cur.execute("CREATE INDEX idxgin ON cve USING gin (data);")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def init_cve_metadata_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='cve_metadata')")
	if not cur.fetchone()[0]:
		cur.execute("create table cve_metadata (metadata_id serial NOT NULL primary key,last_modified_date timestamp, last_modified_date_text text)")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True


def init_jobs_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='jobs')")
	if not cur.fetchone()[0]:
		cur.execute("create table jobs (job_id serial NOT NULL primary key,job_name varchar(255) NOT NULL unique,job_status job_status NOT NULL,job_project_basedir text NOT NULL, job_filename text NOT NULL,job_filepath text NOT NULL,job_notes text)")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def init_data_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='data')")
	if not cur.fetchone()[0]:
		cur.execute("create table data (data_id serial not null primary key,job_id serial references jobs(job_id),data json not null)")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def init_users_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='users')")
	if not cur.fetchone()[0]:
		cur.execute("create table users (user_id serial not null primary key, username text not null, siteadmin boolean not null, hash text not null)")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def init_groups_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='groups')")
	if not cur.fetchone()[0]:
		cur.execute("create table groups (group_id serial not null primary key, groupname text not null)")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def init_user_group_table():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select exists(select * from information_schema.tables where table_name='user_group')")
	if not cur.fetchone()[0]:
		cur.execute("create table user_group (user_id serial not null references users(user_id), group_id serial not null references groups(group_id), read boolean not null, write boolean not null, admin boolean not null )")
		conn.commit()
	cur.close()
	dbclose(conn)
	return True

def put_job(job_name, job_project_basedir, job_filename, job_filepath, job_notes):
	conn = dbconnect()
	if not conn:
		return False
	if get_job_by_name(job_name):
		return False
	cur = conn.cursor()
	job_status = 'queued'
	cur.execute("insert into jobs (job_name,job_status,job_project_basedir,job_filename,job_filepath,job_notes) values (%s,%s,%s,%s,%s,%s) returning job_id", (job_name, job_status, job_project_basedir, job_filename, job_filepath, job_notes))
	job_id = cur.fetchone()[0]
	conn.commit()
	cur.close()
	dbclose(conn)
	return job_id

def put_data(job_id,data):
	conn = dbconnect()
	if not conn:
		return False
	if not get_job_by_id(job_id):
		return False
	cur = conn.cursor()
	cur.execute("insert into data (job_id,data) values (%s,%s) returning data_id", (str(job_id),json.dumps(data)))
	data_id = cur.fetchone()[0]
	conn.commit()
	cur.close()
	dbclose(conn)
	return data_id


def get_job_by_id(job_id):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select job_name,job_status,job_project_basedir,job_filename,job_filepath,job_notes from jobs where job_id=%s",(job_id,))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return {'job_name':result[0],'job_id':job_id,'job_status':result[1],'job_project_basedir':result[2],'job_filename':result[3],'job_filepath':result[4],'job_notes':result[5]}
	else:
		return False

def get_job_by_name(job_name):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select job_id,job_status,job_project_basedir,job_filename,job_filepath,job_notes from jobs where job_name=%s",(job_name,))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return {'job_name':job_name,'job_id':result[0],'job_status':result[1],'job_project_basedir':result[2],'job_filename':result[3],'job_filepath':result[4],'job_notes':result[5]}
	else:
		return False

def get_queued_jobs():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select job_name,job_id,job_status,job_project_basedir,job_filename,job_filepath,job_notes from jobs where job_status=%s",('queued',))
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		new_jobs = []
		for result in results:
			new_jobs.append({'job_name':result[0],'job_id':result[1],'job_status':result[2],'job_project_basedir':result[3],'job_filename':result[4],'job_filepath':result[5],'job_notes':result[6]})
		return new_jobs
	else:
		return False

def get_processing_jobs():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select job_name,job_id,job_status,job_project_basedir,job_filename,job_filepath,job_notes from jobs where job_status=%s",('processing',))
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		new_jobs = []
		for result in results:
			new_jobs.append({'job_name':result[0],'job_id':result[1],'job_status':result[2],'job_project_basedir':result[3],'job_filename':result[4],'job_filepath':result[5],'job_notes':result[6]})
		return new_jobs
	else:
		return False

def get_jobs():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select job_name,job_id,job_status,job_project_basedir,job_filename,job_filepath,job_notes from jobs")
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		jobs = []
		for result in results:
			jobs.append({'job_name':result[0],'job_id':result[1],'job_status':result[2],'job_project_basedir':result[3],'job_filename':result[4],'job_filepath':result[5],'job_notes':result[6]})
		return jobs
	else:
		return False

def get_data(job_id):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select jobs.job_name,jobs.job_status,jobs.job_project_basedir,jobs.job_filename,jobs.job_filepath,jobs.job_notes,data.data_id,data.data from data join jobs on data.job_id=jobs.job_id where jobs.job_id=%s",(str(job_id),))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	if result:
		return {'job_name':result[0],'job_status':result[1],'job_project_basedir':result[2],'job_filename':result[3],'job_filepath':result[4],'job_notes':result[5],'data_id':result[6],'data':result[7]}
	else:
		return False
	
def delete_job_and_data(job_id):
	# delete data then job
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select data.data_id from data join jobs on data.job_id=jobs.job_id where jobs.job_id=%s",(str(job_id),))
	result = cur.fetchone()
	if result:
		data_id = result[0]
		cur.execute("delete from watchdog_3p_cve where data_id=%s",(str(data_id),))
	cur.execute("delete from data where job_id=%s",(job_id,))
	cur.execute("delete from jobs where job_id=%s",(job_id,))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def drop_tables():
	# delete data then job
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("DROP TABLE IF EXISTS watchdog_3p_cve")
	conn.commit()
	cur.execute("DROP TABLE IF EXISTS cve")
	conn.commit()
	cur.execute("DROP TABLE IF EXISTS cve_metadata")
	conn.commit()
	cur.execute("DROP TABLE IF EXISTS data")
	conn.commit()
	cur.execute("DROP TABLE IF EXISTS jobs")
	conn.commit()
	cur.execute("DROP TABLE IF EXISTS user_group")
	conn.commit()
	cur.execute("DROP TABLE IF EXISTS users")
	conn.commit()
	cur.execute("DROP TABLE IF EXISTS groups")
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def drop_enums():
	# delete data then job
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("DROP TYPE job_status")
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def drop_tables_and_enums():
	dt = drop_tables()
	de = drop_enums()
	return dt and de

def set_job_status(job_id, job_status):
	job_id = str(job_id)
	valid_job_status = ['queued', 'processing', 'done']
	if job_status not in valid_job_status:
		return False
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("UPDATE jobs SET job_status=%s WHERE job_id=%s", (job_status, job_id))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def get_user(username):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select username,siteadmin,hash from users where username=%s",(str(username),))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	print(result)
	if result:
		return {'username':result[0],'siteadmin':result[1],'hash':result[2]}
	else:
		return False

def get_user_by_id(user_id):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select username,siteadmin,hash from users where user_id=%s",(str(user_id),))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	print(result)
	if result:
		return {'username':result[0],'siteadmin':result[1],'hash':result[2]}
	else:
		return False

def get_group(groupname):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select group_id,groupname from groups where groupname=%s",(str(groupname),))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	print(result)
	if result:
		return {'group_id':result[0],'groupname':result[1]}
	else:
		return False

def get_group_by_id(group_id):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select group_id,groupname from groups where group_id=%s",(str(group_id),))
	result = cur.fetchone()
	cur.close()
	dbclose(conn)
	print(result)
	if result:
		return {'group_id':result[0],'groupname':result[1]}
	else:
		return False

def get_users():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select user_id,username,siteadmin from users")
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	users = []
	if results:
		for result in results:
			users.append({'user_id':result[0],'username':result[1],'siteadmin':result[2]})
	return users

def get_groups():
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select group_id,groupname from groups")
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	groups = []
	if results:
		for result in results:
			groups.append({'group_id':result[0],'groupname':result[1]})
	return groups

def create_user(username,password,siteadmin):
	conn = dbconnect()
	if not conn:
		return False
	if get_user(username):
		return False
	password = password.encode('utf8')
	pwhash = bcrypt.hashpw(password, bcrypt.gensalt())
	pwhash = pwhash.decode('utf-8')
	cur = conn.cursor()
	cur.execute("insert into users (username,siteadmin,hash) values (%s,%s,%s) returning user_id", (username,siteadmin,pwhash))
	user_id = cur.fetchone()[0]
	conn.commit()
	cur.close()
	dbclose(conn)
	return user_id

def create_group(groupname):
	conn = dbconnect()
	if not conn:
		return False
	if get_group(groupname):
		return False
	cur = conn.cursor()
	cur.execute("insert into groups (groupname) values (%s) returning group_id", (groupname,))
	group_id = cur.fetchone()[0]
	conn.commit()
	cur.close()
	dbclose(conn)
	return group_id

def delete_user(user_id):
	conn = dbconnect()
	if not conn:
		return False
	if not get_user_by_id(user_id):
		return False
	cur = conn.cursor()
	cur.execute("delete from user_group where user_id=%s", (user_id,))
	conn.commit()
	cur.execute("delete from users where user_id=%s", (user_id,))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def delete_group(group_id):
	conn = dbconnect()
	if not conn:
		return False
	if not get_group_by_id(group_id):
		return False
	cur = conn.cursor()
	# TODO: also delete jobs and data that is tied to the group... or make it unassociated... hmmmmmm
	cur.execute("delete from user_group where group_id=%s", (group_id,))
	conn.commit()
	cur.execute("delete from groups where group_id=%s", (group_id,))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def create_user_group(user_id,group_id,read,write,admin):
	conn = dbconnect()
	if not conn:
		return False
	if not get_user_by_id(user_id) or not get_group_by_id(group_id):
		return False
	cur = conn.cursor()
	cur.execute("select user_id from users where user_id=%s", (user_id,))
	result = cur.fetchone()
	if not result:
		return False

	cur.execute("select group_id from groups where group_id=%s", (group_id,))
	result = cur.fetchone()
	if not result:
		return False

	cur = conn.cursor()
	cur.execute("insert into user_group (user_id,group_id,read,write,admin) values (%s,%s,%s,%s,%s)", (user_id,group_id,read,write,admin))
	conn.commit()
	cur.close()
	dbclose(conn)
	return True

def get_job_id_cves(job_id):
	conn = dbconnect()
	if not conn:
		return False
	cur = conn.cursor()
	cur.execute("select cve.data,watchdog_3p_cve.data_id,watchdog_3p_cve.cve_id,watchdog_3p_cve.discover_date,watchdog_3p_cve.last_modified_date_text,watchdog_3p_cve.new,watchdog_3p_cve.rule_matches,watchdog_3p_cve.match_id from watchdog_3p_cve join cve on cve.cve_id=watchdog_3p_cve.cve_id join data on data.data_id=watchdog_3p_cve.data_id WHERE data.job_id=%s",(job_id,))
	results = cur.fetchall()
	cur.close()
	dbclose(conn)
	if results:
		cves_low = []
		cves_medium = []
		cves_high = []
		cves_critical = []
		for result in results:
			cve = {'cve':result[0],'data_id':result[1],'cve_id':result[2],'discover_date':result[3],'last_modified_date_text':result[4],'new':result[5],'rule_matches':result[6],'match_id':result[7]}
			if 'baseMetricV3' in cve['cve']['impact']:
				severity = cve['cve']['impact']['baseMetricV3']['cvssV3']['baseSeverity']
			elif 'baseMetricV2' in cve['cve']['impact']:
				severity = cve['cve']['impact']['baseMetricV2']['severity']
			else:
				severity = 'LOW'

			if severity == 'LOW':
				cves_low.append(cve)
			if severity == 'MEDIUM':
				cves_medium.append(cve)
			if severity == 'HIGH':
				cves_high.append(cve)
			if severity == 'CRITICAL':
				cves_critical.append(cve)
		return cves_critical + cves_high + cves_medium + cves_low
	else:
		return []
	
