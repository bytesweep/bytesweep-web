#!/usr/bin/env python3
from flask import Flask, render_template, Response, request, redirect, make_response, url_for, send_file, jsonify
from werkzeug.utils import secure_filename
from model import *
from fstree import *
from ruamel.yaml import YAML
import json
import os
import uuid
import urllib
import flask_login
import bcrypt
import hashlib

app = Flask(__name__)
project_name = 'ByteSweep'
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
config_file = open('/etc/bytesweep/config.yaml','r')
yaml=YAML()
config = yaml.load(config_file.read())
config_file.close()
app.config['UPLOAD_DIR'] = config['upload_dir']
app.secret_key = config['flask_secret_key']
app.jinja_env.trim_blocks = True
app.jinja_env.lstrip_blocks = True
# if no users exist, create default admin user
if not db_is_setup():
	print('init_tables: '+str(init_tables()))
if len(get_users()) == 0:
	print('create user:pass "admin:pass": '+str(create_user('admin','pass',True)))
#	print('create user:pass "user:pass": '+str(create_user('user','pass',True)))


# https://stackoverflow.com/questions/22669447/how-to-return-a-relative-uri-location-header-with-flask
# https://en.wikipedia.org/wiki/HTTP_location
def relative_redirect(path):
	resp = jsonify()
	resp.status_code = 302
	resp.headers['location'] = path
	resp.autocorrect_location_header = False
	return resp

####### LOGIN STUFF ########
class User(flask_login.UserMixin):
	def __init__(self):
		self.siteadmin = False
		flask_login.UserMixin.__init__(self)

@login_manager.user_loader
def user_loader(username):
	u = get_user(username)
	if not u:
		return
	else:
		user = User()
		user.id = username
		user.siteadmin = u['siteadmin']
		return user

@app.route('/login', methods=['GET', 'POST'])
def login():
	if flask_login.current_user.is_authenticated:
		return relative_redirect('/')

	if request.method == 'GET':
		return render_template('login.html', project_name=project_name)
	
	if 'username' in request.form and 'password' in request.form:
		username = request.form['username']
		u = get_user(username)
		if u:
			submitted_password = request.form['password'].encode('utf8')
			# check password hash
			if bcrypt.checkpw(submitted_password,u['hash'].encode('utf-8')):
				user = User()
				user.id = username
				remember = False
				if 'remember-me' in request.form:
					if request.form['remember-me'] == 'remember-me':
						remember = True
				#this is where flask_login sets the session token
				flask_login.login_user(user,remember)
				return relative_redirect('/')

	return render_template('login.html', project_name=project_name, error='Invalid Login')


	if username in users:
		# TODO: kick this decision to model.py or somewhere else
		if request.form['password'] == users[username]['password']:
			user = User()
			user.id = username
			remember = False
			if 'remember-me' in request.form:
				if request.form['remember-me'] == 'remember-me':
					remember = True
			#this is where flask_login sets the session token
			flask_login.login_user(user,remember)
			return relative_redirect('/')
	return render_template('login.html', project_name=project_name, error='Invalid Login')

@app.route('/logout')
def logout():
	flask_login.logout_user()
	return relative_redirect('/')

# TODO: maybe move to helpers.py file?
def get_current_job(request):
	job = False
	if 'id' in request.args:
		job_id = urllib.parse.quote_plus(request.args['id'])
		job = get_job_by_id(job_id)
	elif 'current_id' in request.cookies:
		job_id = urllib.parse.quote_plus(request.cookies['current_id'])
		job = get_job_by_id(job_id)
	return job

@app.route('/')
def index():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)
	if job:
		job_name = job['job_name']
	else:
		job_name = ''
	return render_template('landing.html', project_name=project_name, job_name=job_name, admin=flask_login.current_user.siteadmin)

@app.route('/admin/landing')
def admin_main():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')
	if not flask_login.current_user.siteadmin:
		return relative_redirect('/')

	return render_template('admin-landing.html', project_name=project_name)

@app.route('/about')
def about():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)
	if job:
		job_name = job['job_name']
	else:
		job_name = ''
	return render_template('about.html', project_name=project_name, job_name=job_name, admin=flask_login.current_user.siteadmin)

@app.route('/addartifact', methods = ['POST', 'GET'])
def addartifact():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	if request.method == 'POST':
		# check for required form fields
		if 'artifactname' not in request.form:
			return render_template('addartifact.html', project_name=project_name, error=True, error_msg='missing required form fields', admin=flask_login.current_user.siteadmin)

		# POST fields
		artifactname = request.form['artifactname']
		if 'notes' in request.form:
			notes = request.form['notes']
		else:
			notes = ''

		# ensure required fields are filled in
		if artifactname == '':
			return render_template('addartifact.html', project_name=project_name, error=True, error_msg='missing required form fields', admin=flask_login.current_user.siteadmin)

		# handle file upload
		# check if the post request has the file part
		if 'file_upload' not in request.files:
			return render_template('addartifact.html', project_name=project_name, error=True, error_msg='missing required form fields', admin=flask_login.current_user.siteadmin)
		file_upload = request.files['file_upload']
		if file_upload.filename == '':
			return render_template('addartifact.html', project_name=project_name, error=True, error_msg='missing required form fields', admin=flask_login.current_user.siteadmin)
		# TODO: implement check for dup artifact name
		if file_upload:
			filename = secure_filename(file_upload.filename)
			try:
				while True:
					uuid_string = str(uuid.uuid4())
					project_basedir = os.path.join(app.config['UPLOAD_DIR'], uuid_string)
					filepath = os.path.join(project_basedir, filename)
					if not os.path.exists(project_basedir):
						os.makedirs(project_basedir)
						break

				file_upload.save(filepath)
			except:
				return render_template('addartifact.html', project_name=project_name, error=True, error_msg='file upload failed', admin=flask_login.current_user.siteadmin)

		# insert stuff into DB
		job_id = put_job(artifactname, project_basedir, filename, filepath, notes)

		url = '/analysis/overview?id=' + str(job_id)
		return relative_redirect(url)
	else:
		# GET
		job = get_current_job(request)
		if job:
			job_name = job['job_name']
		else:
			job_name = ''
		return render_template('addartifact.html', project_name=project_name, error=False, job_name=job_name, admin=flask_login.current_user.siteadmin)

@app.route('/listartifacts')
def listartifacts():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)
	if job:
		job_name = job['job_name']
	else:
		job_name = ''
	jobs = get_jobs()
	# TODO why was get_jobs() returning False???
	if not jobs:
		jobs = []
	return render_template('listartifacts.html', project_name=project_name, jobs=jobs, job_name=job_name, admin=flask_login.current_user.siteadmin)

@app.route('/admin/users')
def admin_users():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')
	if not flask_login.current_user.siteadmin:
		return relative_redirect('/')
	users = get_users()
	groups = get_groups()
	return render_template('admin-users.html', project_name=project_name, users=users, groups=groups, admin=flask_login.current_user.siteadmin)

@app.route('/admin/groups')
def admin_groups():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')
	if not flask_login.current_user.siteadmin:
		return relative_redirect('/')
	groups = get_groups()
	return render_template('admin-groups.html', project_name=project_name, groups=groups, admin=flask_login.current_user.siteadmin)

@app.route('/admin/adduser', methods = ['POST'])
def admin_adduser():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')
	if not flask_login.current_user.siteadmin:
		return relative_redirect('/')
	if 'username' not in request.form or 'password' not in request.form:
		return relative_redirect('/admin/users')
	username = request.form['username']
	password = request.form['password']
	siteadmin = False
	if 'siteadmin' in request.form:
		if request.form['siteadmin'] == 'on':
			siteadmin = True
	if get_user(username):
		# return an error message: user already exists
		return relative_redirect('/admin/users')
	groups = []
	if 'group_ids' in request.form:
		group_ids = request.form['group_ids']
		for group_id in group_ids:
			g = get_group_by_id(group_id)
			if g:
				if 'group_'+group_id+'_perms' in request.form:
					perm = request.form['group_'+group_id+'_perms']
					groups.append({'group_id':group_id,'perm':perm})
	# create user
	user_id = create_user(username,password,siteadmin)
	if not user_id:
		# return an error message: user create failed
		return relative_redirect('/admin/users')
	for group in groups:
		read = False
		write = False
		admin = False
		if group['perm'] == 'read':
			read = True
		if group['perm'] == 'write':
			write = True
		if group['perm'] == 'admin':
			admin = True
		create_user_group(user_id,group['group_id'],read,write,admin)

		# return
		return relative_redirect('/admin/users')

@app.route('/admin/addgroup', methods = ['POST'])
def admin_addgroup():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')
	if not flask_login.current_user.siteadmin:
		return relative_redirect('/')

	if 'groupname' not in request.form:
		return relative_redirect('/admin/groups')

	groupname = request.form['groupname']
	# create group
	group_id = create_group(groupname)
	if not group_id:
		# return an error message: user create failed
		return relative_redirect('/admin/groups')

	# return
	return relative_redirect('/admin/groups')

@app.route('/admin/deleteuser', methods = ['POST'])
def admin_deleteuser():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')
	if not flask_login.current_user.siteadmin:
		return relative_redirect('/')

	if 'user_id' not in request.form:
		return relative_redirect('/admin/users')

	user_id = request.form['user_id']
	# delete user
	result = delete_user(user_id)
	if not result:
		# return an error message: user create failed
		return relative_redirect('/admin/users')
	# return
	return relative_redirect('/admin/users')

@app.route('/admin/deletegroup', methods = ['POST'])
def admin_deletegroup():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')
	if not flask_login.current_user.siteadmin:
		return relative_redirect('/')

	if 'group_id' not in request.form:
		return relative_redirect('/admin/groups')

	group_id = request.form['group_id']
	# delete group
	result = delete_group(group_id)
	if not result:
		# return an error message: group delete failed
		return relative_redirect('/admin/groupss')
	# return
	return relative_redirect('/admin/groups')

@app.route('/deleteartifact', methods = ['POST'])
def deleteartifact():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)
	resp = make_response(relative_redirect('/listartifacts'))
	if 'id' in request.form:
		result = delete_job_and_data(request.form['id'])
		if job:
			if 'job_id' in job:
				if result and request.form['id'] == job['job_id']:
					resp.set_cookie('current_id', '', expires=0)
	return resp

@app.route('/analysis/overview')
def analysis_overview():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)
	if job:
		job_name = job['job_name']
		if job['job_status'] == 'done':
			data = get_data(job['job_id'])
			job['extraction_time'] = data['data']['extraction_time']
			job['unsafe_libs_time'] = data['data']['unsafe_libs_time']
			job['string_analysis_time'] = data['data']['string_analysis_time']
			stats = data['data']['stats']
			sorted_cves = {}
			cves = get_job_id_cves(job['job_id'])
			for cve in cves:
				if not cve['cve_id'] in sorted_cves:
					sorted_cves[cve['cve_id']] = cve
					sorted_cves[cve['cve_id']]['stringdata'] = []
				sorted_cves[cve['cve_id']]['stringdata'].append(1)
			cves = list(sorted_cves.values())
			stats['cves'] = len(cves)
		else:
			stats = []
		resp = make_response(render_template('analysis-overview.html', project_name=project_name, job=job, job_name=job_name, stats=stats, admin=flask_login.current_user.siteadmin))
		resp.set_cookie('current_id', job['job_id'])
		return resp
	else:
		return render_template('noartifact.html', project_name=project_name, admin=flask_login.current_user.siteadmin)

@app.route('/analysis/files/dir')
def analysis_files_dir():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)

	if job:
		job_name = job['job_name']
		if job['job_status'] == 'done':
			data = get_data(job['job_id'])
			fid = 0
			if 'fid' in request.args:
				fid = urllib.parse.quote_plus(request.args['fid'])
			if data:
				fstree = list(fstree_iterate(data['data']['fstree']))
				# TODO: error check for non-int.... or non-dir
				node = fstree[int(fid)]
				branch_path = []
				for b in node['node']['branch_path']:
					bp = {'fid':b,'name':fstree[b]['node']['name'],'leaf':False}
					branch_path.append(bp)
				branch_path.append({'fid':node['node']['fid'],'name':node['node']['name'],'leaf':True})
				return render_template('analysis-files-dir.html', project_name=project_name, job=job, node=node, branch_path=branch_path, job_name=job_name, admin=flask_login.current_user.siteadmin)
			else:
				# TODO: change to 500
				return render_template('404.html', project_name=project_name, admin=flask_login.current_user.siteadmin), 404
		else:
			return render_template('analysis-files-dir.html', project_name=project_name, job=job, job_name=job_name, admin=flask_login.current_user.siteadmin)
	else:
		return render_template('noartifact.html', project_name=project_name, admin=flask_login.current_user.siteadmin)

@app.route('/analysis/files/file')
def analysis_files_file():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')
	job = get_current_job(request)
	if job:
		job_name = job['job_name']
		if job['job_status'] == 'done':
			data = get_data(job['job_id'])
			fid = 1
			if 'fid' in request.args:
				fid = urllib.parse.quote_plus(request.args['fid'])
			if data:
				fstree = list(fstree_iterate(data['data']['fstree']))
				# TODO: error check for non-int.... or non-file
				node = fstree[int(fid)]
				branch_path = []
				for b in node['node']['branch_path']:
					bp = {'fid':b,'name':fstree[b]['node']['name'],'leaf':False}
					branch_path.append(bp)
				branch_path.append({'fid':node['node']['fid'],'name':node['node']['name'],'leaf':True})
				return render_template('analysis-files-file.html', project_name=project_name, job=job, node=node, branch_path=branch_path, job_name=job_name, admin=flask_login.current_user.siteadmin)
			else:
				# TODO: change to 500
				return render_template('404.html', project_name=project_name, admin=flask_login.current_user.siteadmin), 404
		else:
			return render_template('analysis-files-file.html', project_name=project_name, job=job, job_name=job_name, admin=flask_login.current_user.siteadmin)
	else:
		return render_template('noartifact.html', project_name=project_name, admin=flask_login.current_user.siteadmin)

@app.route('/analysis/keys')
def analysis_keys():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)
	if job:
		job_name = job['job_name']
		if job['job_status'] == 'done':
			data = get_data(job['job_id'])
			if data:
				keys = {}
				stringdata = data['data']['string_analysis']
				for string in stringdata:
					if string['type'] == 'crypto-keys':
						keyhash = hashlib.sha256(string['match'].encode('utf-8')).hexdigest()
						if keyhash not in keys:
							keys[keyhash] = []
						keys[keyhash].append(string)
				return render_template('analysis-keys.html', project_name=project_name, job=job, keys=list(keys.values()), job_name=job_name, admin=flask_login.current_user.siteadmin)
			else:
				# TODO: change to 500
				return render_template('404.html', project_name=project_name, admin=flask_login.current_user.siteadmin), 404
		else:
			return render_template('analysis-keys.html', project_name=project_name, job=job, job_name=job_name, admin=flask_login.current_user.siteadmin)
	else:
		return render_template('noartifact.html', project_name=project_name, admin=flask_login.current_user.siteadmin)

@app.route('/analysis/passwords')
def analysis_passwords():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)
	if job:
		job_name = job['job_name']
		if job['job_status'] == 'done':
			data = get_data(job['job_id'])
			if data:
				passwords = {}
				stringdata = data['data']['string_analysis']
				for string in stringdata:
					if string['type'] == 'password':
						phash = hashlib.sha256(string['match'].encode('utf-8')).hexdigest()
						if phash not in passwords:
							passwords[phash] = []
						passwords[phash].append(string)
				return render_template('analysis-passwords.html', project_name=project_name, job=job, passwords=list(passwords.values()), job_name=job_name, admin=flask_login.current_user.siteadmin)
			else:
				# TODO: change to 500
				return render_template('noartifact.html', project_name=project_name, admin=flask_login.current_user.siteadmin)
		else:
			return render_template('analysis-passwords.html', project_name=project_name, job=job, job_name=job_name, admin=flask_login.current_user.siteadmin)
	else:
		return render_template('noartifact.html', project_name=project_name, admin=flask_login.current_user.siteadmin)


@app.route('/analysis/3pcomponents')
def analysis_3pcomponents():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)
	if job:
		job_name = job['job_name']
		if job['job_status'] == 'done':
			data = get_data(job['job_id'])
			if data:
				components = {}
				stringdata = data['data']['string_analysis']
				for string in stringdata:
					if string['type'] == 'program version' or string['type'] == 'library version':
						component = {'type':string['type'],'version':string['match']}
						chash = hashlib.sha256(json.dumps(component).encode('utf-8')).hexdigest()
						if chash not in components:
							components[chash] = []
						components[chash].append(string)
				return render_template('analysis-3pcomponents.html', project_name=project_name, job=job, components=list(components.values()), job_name=job_name, admin=flask_login.current_user.siteadmin)
			else:
				# TODO: change to 500
				return render_template('404.html', project_name=project_name, admin=flask_login.current_user.siteadmin), 404
		else:
			return render_template('analysis-3pcomponents.html', project_name=project_name, job=job, job_name=job_name, admin=flask_login.current_user.siteadmin)
	else:
		return render_template('noartifact.html', project_name=project_name, admin=flask_login.current_user.siteadmin)

@app.route('/analysis/functions')
def analysis_functions():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)
	if job:
		job_name = job['job_name']
		if job['job_status'] == 'done':
			data = get_data(job['job_id'])
			if data:
				filedata = []
				fstree = data['data']['fstree']
				for extracted_file in fstree_iterate(fstree):
					if 'unsafe_functions' in extracted_file['node']:
						filedata.append(extracted_file['node'])
				return render_template('analysis-functions.html', project_name=project_name, job=job, filedata=filedata, job_name=job_name, admin=flask_login.current_user.siteadmin)
			else:
				# TODO: change to 500
				return render_template('404.html', project_name=project_name, admin=flask_login.current_user.siteadmin), 404
		else:
			return render_template('analysis-functions.html', project_name=project_name, job=job, job_name=job_name, admin=flask_login.current_user.siteadmin)
	else:
		return render_template('noartifact.html', project_name=project_name, admin=flask_login.current_user.siteadmin)

# use this get_job_id_cves
@app.route('/analysis/watchdog')
def analysis_watchdog():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)
	if job:
		job_name = job['job_name']
		if job['job_status'] == 'done':
			cves = get_job_id_cves(job['job_id'])
			data = get_data(job['job_id'])
			stringdata = data['data']['string_analysis']

			sorted_cves = {}
			for cve in cves:
				if not cve['cve_id'] in sorted_cves:
					sorted_cves[cve['cve_id']] = cve
					sorted_cves[cve['cve_id']]['stringdata'] = []
				sorted_cves[cve['cve_id']]['stringdata'].append(stringdata[int(cve['match_id'])])
			cves = list(sorted_cves.values())

			if cves:
				return render_template('analysis-watchdog.html', project_name=project_name, job=job, cves=cves, job_name=job_name, admin=flask_login.current_user.siteadmin)
			else:
				return render_template('analysis-watchdog.html', project_name=project_name, job=job, cves=[], job_name=job_name, admin=flask_login.current_user.siteadmin)
		else:
			return render_template('analysis-watchdog.html', project_name=project_name, job=job, job_name=job_name, admin=flask_login.current_user.siteadmin)
	else:
		return render_template('noartifact.html', project_name=project_name, admin=flask_login.current_user.siteadmin)

@app.route('/download/file')
def download_file():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	job = get_current_job(request)
	if 'fid' not in request.args:
		return render_template('404.html', project_name=project_name), 404

	fid = request.args['fid']
	if job:
		job_name = job['job_name']
		if job['job_status'] == 'done':
			data = get_data(job['job_id'])
			if data:
				fstree = list(fstree_iterate(data['data']['fstree']))
				return send_file(fstree[int(fid)]['node']['path'], as_attachment=True)
			else:
				# TODO: change to 500
				return render_template('404.html', project_name=project_name, admin=flask_login.current_user.siteadmin), 404
		else:
			return render_template('analysis-files.html', project_name=project_name, job=job, job_name=job_name, admin=flask_login.current_user.siteadmin)
	else:
		return render_template('404.html', project_name=project_name, admin=flask_login.current_user.siteadmin), 404

@app.route('/download/string')
def download_string():
	# page is behind login
	if not flask_login.current_user.is_authenticated:
		return relative_redirect('/login')

	if 'match_id' not in request.args:
		return render_template('404.html', project_name=project_name), 404
	match_id = request.args['match_id']

	job = get_current_job(request)
	if job:
		job_name = job['job_name']
		if job['job_status'] == 'done':
			data = get_data(job['job_id'])
			if data:
				stringdata = data['data']['string_analysis']
				for string in stringdata:
					
					if match_id == str(string['match_id']):
						#download string
						filename = 'string'+match_id
						resp = make_response(string['match'])
						resp.headers["Content-Disposition"] = "attachment; filename="+filename
						return resp
	return render_template('404.html', project_name=project_name, admin=flask_login.current_user.siteadmin), 404

@app.route('/static/<path:path>')
def static_content(path):
    return send_from_directory('static', path)
 
if __name__ == "__main__":
	# TODO: this is bad... make this prod ready
	app.run(host='0.0.0.0',port=5000)
