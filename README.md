# bytesweep-web

Web frontend for the ByteSweep platform.

## INSTALL

### Ubuntu 18.04

install deps:
```
apt-get install python3-ruamel.yaml python3-flask python3-flask-login python3-werkzeug python3-urllib3 python3-psycopg2 python3-bcrypt python3-gunicorn
```
